<?php

namespace Drupal\prometheus_exporter_database_tablesize\Plugin\MetricsCollector;

use Drupal\Core\Database\Connection;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\prometheus_exporter\Plugin\BaseMetricsCollector;
use PNX\Prometheus\Gauge;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Collects metrics for MySQL database table sizes.
 *
 * @MetricsCollector(
 *   id = "mysql_database_table_size",
 *   title = @Translation("MySQL database table size"),
 *   description = @Translation("Provides metrics for database table sizes (in bytes) for a MySQL Drupal database.")
 * )
 */
class MySqlDatabaseTableSizeCollector extends BaseMetricsCollector implements ContainerFactoryPluginInterface {

  /**
   * The PDO database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * UpdateStatusCollector constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Database\Connection $connection
   *   The PDO database connection.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $connection) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function collectMetrics() {
    $gauge = new Gauge($this->getNamespace(), 'total', $this->getDescription());
    foreach ($this->query() as $table => $size) {
      $gauge->set($size, ['table' => $table]);
    }
    return [$gauge];
  }

  /**
   * Query the MySQL catalogue to fetch the table size for the defined tables.
   *
   * @return array
   *   Associative array of table sizes (in bytes) indexed by their table name.
   */
  protected function query() {
    $tables = $this->prefixTables($this->getTables());
    $query = $this->connection->query('SELECT TABLE_NAME, (DATA_LENGTH + INDEX_LENGTH) AS `size` FROM information_schema.TABLES WHERE TABLE_NAME IN (:tables[])', [':tables[]' => $tables]);
    return $query->fetchAllKeyed(0, 1);
  }

  /**
   * Fetch a list of database tables.
   *
   * @return array
   *   An array of database tables for this Drupal database.
   */
  protected function getTables() {
    return array_values($this->connection->schema()->findTables('%'));
  }

  /**
   * Add the appropriate table prefixes as needed.
   *
   * @param array $tables
   *   An array of table names for Drupal database tables.
   *
   * @return array
   *   The table names, with the appropriate DB prefix added.
   */
  protected function prefixTables($tables) {
    return array_map(function ($table) {
      return $this->connection->prefixTables('{' . $table . '}');
    }, $tables);
  }

}
