<?php

namespace Drupal\prometheus_exporter_advancedqueue\Plugin\MetricsCollector;

use Drupal\advancedqueue\Job;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\prometheus_exporter\Plugin\BaseMetricsCollector;
use PNX\Prometheus\Gauge;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Collects metrics for queue sizes.
 *
 * @MetricsCollector(
 *   id = "advanced_queue_size",
 *   title = @Translation("Advanced queue size"),
 *   description = @Translation("Provides metrics for queue sizes for the advanced queue module.")
 * )
 */
class AdvancedQueueSizeCollector extends BaseMetricsCollector implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * UpdateStatusCollector constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function collectMetrics() {
    $gauge = new Gauge($this->getNamespace(), 'total', $this->getDescription());

    $metrics = [];
    $queue_storage = $this->entityTypeManager->getStorage('advancedqueue_queue');
    foreach ($queue_storage->loadMultiple() as $queue) {
      foreach ($queue->getBackend()->countJobs() as $state => $count) {
        $gauge->set($count, [
          'queue' => $queue->id(),
          'queue_label' => $queue->label(),
          'state' => $state,
          'state_label' => self::labels()[$state]->render(),
        ]);
      }
    }

    $metrics[] = $gauge;
    return $metrics;
  }

  /**
   * Fetch the labels for each job state.
   *
   * @return array
   *   An array of translatable labels, indexed by their state key.
   */
  protected static function labels() {
    return [
      Job::STATE_QUEUED => new TranslatableMarkup('Queued'),
      Job::STATE_PROCESSING => new TranslatableMarkup('Processing'),
      Job::STATE_SUCCESS => new TranslatableMarkup('Success'),
      Job::STATE_FAILURE => new TranslatableMarkup('Failure'),
    ];
  }

}
