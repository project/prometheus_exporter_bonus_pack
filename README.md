# Prometheus Exporter Bonus Pack

The Prometheus Exporter Bonus Pack exposes metrics for contrib modules and
use-cases considered too niche for the main Prometheus Exporter module.

## Metrics provided

- Database table-size (MySQL only)

- Queue size for the *Advanced Queue* module.
